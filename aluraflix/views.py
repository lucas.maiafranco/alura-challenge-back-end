from rest_framework import viewsets
from aluraflix import serializer
from aluraflix.models import Video
from aluraflix.serializer import VideoSerializer

class VideoViewSet(viewsets.ModelViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer